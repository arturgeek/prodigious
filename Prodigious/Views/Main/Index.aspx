﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <meta name="viewport" content="width=device-width" />
    <title>Index</title>

    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="../../Js/bootstrap.min.js"></script>
    <script src="../../Js/controller.js"></script>
    <script src="../../Js/jquery-ui.min.js"></script>

    <link href="../../style/bootstrap.css" rel="stylesheet" />
    <link href="../../style/bootstrap-theme.css" rel="stylesheet" />
    <link href="../../style/Main.css" rel="stylesheet" />
    <link href="../../style/jquery-ui.min.css" rel="stylesheet" />
    <link href="../../style/jquery-ui.theme.min.css" rel="stylesheet" />

    <script>
        
        $(function () {
            $("#txtSellStart").datepicker({ dateFormat: "yy-mm-dd" });
            $("#txtSellEnd").datepicker({ dateFormat: "yy-mm-dd" });
            $("#txtDiscontinuate").datepicker({ dateFormat: "yy-mm-dd" });
        });

    </script>
</head>
<body>
    <div class="container" >
        <div class="row">
            <div class="col-md-12 bg-primary">
                <h1>Prodigious Products Manager</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center" id="pnlLoading">
                <img src="../../images/loader.gif" alt="Loading..." />
                <div class="bg-danger txtError"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="pnlData">
                    <table class="table table-responsive">
                        <thead>
                            <tr>
                                <td></td>
                                <td>Name</td>
                                <td>Number</td>
                                <td>Color</td>
                            </tr>
                        </thead>
                        <tbody id="tbData">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <h3 class="bg-primary">Create Product</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <div class="form-group">
                    <label>Name:</label>
                    <input class="form-control" type='text' id='txtName' />
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Number:</label>
                    <input class="form-control" type='text' id='txtNumber' />
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Color:</label>
                    <input class="form-control" type='text' id='txtColor' />
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Standar Cost:</label>
                    <input class="form-control" type='text' id='txtStandarCost' />
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>List price:</label>
                    <input class="form-control" type='text' id='txtListPrice' />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <div class="form-group">
                    <label>Size:</label>
                    <input class="form-control" type='text' id='txtSize' />
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Weight:</label>
                    <input class="form-control" type='text' id='txtWeight' />
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Category:</label>
                    <select class="form-control" id="ddCategory">
                        <option value="0">Seleccionar</option>
                    </select>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Sub Category:</label>
                    <select class="form-control" id="ddSubCategory"></select>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Model:</label>
                    <select class="form-control" id="ddModel"></select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <div class="form-group">
                    <label>Sell Start Date:</label>
                    <input class="form-control" type='text' id='txtSellStart' />
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Sell End Date:</label>
                    <input class="form-control" type='text' id='txtSellEnd' />
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Discontinuate Date:</label>
                    <input class="form-control" type='text' id='txtDiscontinuate' />
                </div>
            </div>
            <div class="col-md-2">
                <br />
                <button class="btn btn-primary" id="btnCreate">Create</button>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalProduct" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span></button>
                    <h4 class="modal-title" id="myModalLabel">Product information</h4>
                </div>
                <div class="modal-body">
                    <div id="productData">


                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
