﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Prodigious.Models
{
    public class ProductManager
    {
        Prodigious.Models.prodigiousEntities ddbb = new Models.prodigiousEntities();
        public ProductManager()
        {
            ddbb.Configuration.ProxyCreationEnabled = false;
        }

        public List<CategoryEntity> GetCategory()
        {
            return ddbb.ProductCategory.AsEnumerable().Select(c => new CategoryEntity{
                ProductCategoryID = c.ProductCategoryID,
                Name = c.Name,
                ParentProductCategoryID = c.ParentProductCategoryID
            }).Where(
                c => c.ParentProductCategoryID == null
            ).ToList();
        }

        public List<CategoryEntity> GetSubCategory(int parentProductCategoryID)
        {
            return ddbb.ProductCategory.ToList().Select(c => new CategoryEntity
            {
                ProductCategoryID = c.ProductCategoryID,
                Name = c.Name,
                ParentProductCategoryID = c.ParentProductCategoryID
            }).Where(c => c.ParentProductCategoryID == parentProductCategoryID).ToList();
        }

        public List<Models.ProductModel> GetModels()
        {
            return ddbb.ProductModel.ToList();
        }

        public List<Models.Product> GetAll()
        {
            List<Models.Product> products = ddbb.Product.ToList();
            return products;
        }

        public Models.Product GetProduct(int productId)
        {

            Models.Product product = ddbb.Product.SingleOrDefault(p => p.ProductID == productId);
            return product;
        }

        public Models.Product CreateProduct(
            string Name,
            string ProductNumber,
            string Color,
            double StandardCost,
            double ListPrice,
            string Size,
            double Weight,
            int ProductCategoryID,
            int ProductModelID,
            DateTime SellStartDate,
            DateTime SellEndDate,
            DateTime DiscontinuedDate)
        {
            Models.Product product = new Models.Product();
            product.Name = Name;
            product.ProductNumber = ProductNumber;
            product.Color = Color;
            product.StandardCost = Convert.ToDecimal(StandardCost);
            product.ListPrice = Convert.ToDecimal(ListPrice);
            product.Size = Size;
            product.Weight = Convert.ToDecimal(Weight);
            product.ProductCategoryID = ProductCategoryID;
            product.ProductModelID = ProductModelID;
            product.SellStartDate = SellStartDate;
            product.SellEndDate = SellEndDate;
            product.DiscontinuedDate = DiscontinuedDate;
            product.ModifiedDate = DateTime.Now;
            product.rowguid = Guid.NewGuid();

            ddbb.Product.Add(product);
            ddbb.SaveChanges();

            return product;
        }
    }
}