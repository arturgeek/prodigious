﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Prodigious.Models
{
    public class CategoryEntity
    {
        public int ProductCategoryID { get; set; }
        public string Name { get; set; }
        public int? ParentProductCategoryID { get; set; }
    }
}