﻿$(function () {
    RetrieveCategories();
    RetrieveModels();
    RetrieveProducts();

    $("#ddCategory").on("change", function () {
        RetrieveSubCategories($(this).val());
    });

    $("#btnCreate").on("click", function () {
        ValidateAndCreateProduct();
    });
});

function RetrieveCategories() {
    ExecuteAjax("Main/GetCategory", {}, ShowCategories);
    
}

function RetrieveSubCategories(categoryId) {
    ExecuteAjax("Main/GetSubCategory", { categoryId: categoryId }, ShowSubCategories);
}

function RetrieveModels(categoryId) {
    ExecuteAjax("Main/GetModels", {}, ShowModels);
} 

function RetrieveProducts() {
    ExecuteAjax("Main/GetAllProducts", {}, BuildData);
}

function RetrieveSpecificProduct(id) {
    ExecuteAjax("Main/GetUniqueProduct", { productId: id }, ShowProduct);
}

function ExecuteAjax(url, data, callbackFunction) {
    $("#pnlLoading .txtError").empty();
    $("#pnlLoading img").show();
    $.post(url, data, function (data, textStatus) {
        $("#pnlLoading img").hide();
        CheckResponse(data, textStatus, callbackFunction);
    }, "json")
    .error(function () {
        $("#pnlLoading img").hide();
        alert('Verify that all the fields are complete and correct to proceed33e222222222222');
    });
}

function CheckResponse(data, textStatus, callbackFunction) {
    if (textStatus == "success") {
        if (!data.error) {
            callbackFunction(data);
        }
        else {
            HandleError(data.message);
        }
    }
    else {
        HandleError(textStatus);
    }
    console.log(textStatus);
    console.log(data);
}

function HandleError(error) {
    $("#pnlLoading .txtError").html(error);
}

function BuildData(data) {
    $("#tbData").empty();
    $.each(data.products, function (index, product) {
        var row = Array();

        row.push("<tr>");
        row.push("<td><span class='glyphicon glyphicon-search btnGetElement' data-id='" + product.ProductID + "'></span></td>");
        row.push("<td>" + product.Name + "</td>");
        row.push("<td>" + product.ProductNumber + "</td>");
        row.push("<td>" + product.Color + "</td>");
        row.push("</tr>");

        $("#tbData").append(row.join(""));
    });

    $(".btnGetElement").on("click", function () {
        RetrieveSpecificProduct($(this).data("id"));
    });
}

function ShowProduct(data) {
    $('#modalProduct').modal('show');

    var product = Array();
    product.push("<strong>Name:</strong> " + data.product.Name);
    product.push("<strong>Color:</strong> " + data.product.Color);
    product.push("<strong>StandardCost:</strong> " + data.product.StandardCost);
    product.push("<strong>ListPrice:</strong> " + data.product.ListPrice);
    product.push("<strong>Size:</strong> " + data.product.Size);
    product.push("<strong>Weight:</strong> " + data.product.Weight);

    $("#productData").append(product.join("<br/>"));
}

function ShowCategories(data)
{
    $.each(data.categories, function (index, category) {
        var option = "<option value='" + category.ProductCategoryID + "'>" + category.Name + "</option>";
        $("#ddCategory").append(option);
    });
}

function ShowSubCategories(data) {
    $("#ddSubCategory").empty();
    $.each(data.categories, function (index, category) {
        var option = "<option value='" + category.ProductCategoryID + "'>" + category.Name + "</option>";
        $("#ddSubCategory").append(option);
    });
}

function ShowModels(data) {
    $("#ddModel").empty();
    $.each(data.categories, function (index, category) {
        var option = "<option value='" + category.ProductModelID + "'>" + category.Name + "</option>";
        $("#ddModel").append(option);
    });
}

function ValidateAndCreateProduct()
{
    var Product = {
        Name: $("#txtName").val(),
        ProductNumber: $("#txtNumber").val(),
        Color: $("#txtColor").val(),
        StandardCost: $("#txtStandarCost").val(),
        ListPrice: $("#txtListPrice").val(),
        Size: $("#txtSize").val(),
        Weight: $("#txtWeight").val(),
        ProductCategoryID: $("#ddSubCategory").val(),
        ProductModelID: $("#ddModel").val(),
        SellStartDate: $("#txtSellStart").val(),
        SellEndDate: $("#txtSellEnd").val(),
        DiscontinuedDate: $("#txtDiscontinuate").val()
    };

    ExecuteAjax("Main/CreateProduct", Product, CreationResult);
}

function CreationResult(data)
{
    RetrieveProducts();
    ShowProduct(data);
}