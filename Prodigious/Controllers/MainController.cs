﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Prodigious.Models;

namespace Prodigious.Controllers
{
    public class MainController : Controller
    {
        //
        // GET: /Main/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetCategory()
        {
            try
            {
                ProductManager productManager = new ProductManager();
                return Json(new { error = false, categories = productManager.GetCategory() });
            }
            catch (Exception ex)
            {
                return Json(new { error = true, message = "Error while retrieving data" });
            }
        }

        public ActionResult GetSubCategory(int categoryId)
        {
            try
            {
                ProductManager productManager = new ProductManager();
                return Json(new { error = false, categories = productManager.GetSubCategory(categoryId) });
            }
            catch (Exception ex)
            {
                return Json(new { error = true, message = "Error while retrieving data" });
            }
        }

        public ActionResult GetModels()
        {
            try
            {
                ProductManager productManager = new ProductManager();
                return Json(new { error = false, categories = productManager.GetModels() });
            }
            catch (Exception ex)
            {
                return Json(new { error = true, message = "Error while retrieving data" });
            }
        }

        public ActionResult GetAllProducts()
        {
            try
            {
                ProductManager productManager = new ProductManager();
                return Json(new { error = false, products = productManager.GetAll() });
            }
            catch(Exception ex)
            {
                return Json(new { error = true, message = "Error while retrieving data" });
            }
        }

        public ActionResult GetUniqueProduct(int productId)
        {
            try
            {
                ProductManager productManager = new ProductManager();
                return Json(new { error = false, product = productManager.GetProduct(productId) });
            }
            catch (Exception ex)
            {
                return Json(new { error = true, message = "Error while retrieving data" });
            }
        }

        public ActionResult CreateProduct(
            string Name, 
            string ProductNumber, 
            string Color, 
            double StandardCost, 
            double ListPrice, 
            string Size, 
            double Weight, 
            int ProductCategoryID,
            int ProductModelID,
            DateTime SellStartDate,
            DateTime SellEndDate,
            DateTime DiscontinuedDate)
        {
            try
            {
                ProductManager productManager = new ProductManager();
                Product product = productManager.CreateProduct(Name, ProductNumber, Color, StandardCost, ListPrice, Size, Weight, ProductCategoryID, ProductModelID, SellStartDate, SellEndDate, DiscontinuedDate);

                return Json(new { error = false, product = product});
            }
            catch (Exception ex)
            {
                return Json(new { error = true, message = ex.InnerException.Message });
            }
        }

        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                MaxJsonLength = Int32.MaxValue
            };
        }
    }
}
